from models.PatoRuivo import PatoRuivo
from models.VoarFoguete import VoarFoguete

if __name__ == "__main__":
    pt = PatoRuivo()
    print(pt.mostrar())
    print(pt.nadar())
    print(pt.comportamentoPato())
    pt.setComportamento(VoarFoguete())
    print(pt.comportamentoPato())
