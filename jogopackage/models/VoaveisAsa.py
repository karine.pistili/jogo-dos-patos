from models.PadraoVoaveis import PadraoVoaveis 

class VoaveisAsa(PadraoVoaveis):
    def __init__(self):
        self.velocidade = 10

    def voar(self):
        return f'Voando como um passaro que voa. Velocidade: {self.getVelocidade()}'

    def getVelocidade(self):
        return self.velocidade