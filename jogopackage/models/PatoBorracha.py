from models.Pato import Pato
from models.NaoVoa import NaoVoa

class PatoBorracha(Pato):
    def __init__(self):
        self.setComportamento(NaoVoa())

    def mostrar(self):
        return "Ola, eu sou de Borracha"