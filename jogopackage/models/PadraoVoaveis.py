import abc

class PadraoVoaveis:
    @abc.abstractmethod
    def voar(self):
        pass

    @abc.abstractmethod
    def getVelocidade(self):
        pass