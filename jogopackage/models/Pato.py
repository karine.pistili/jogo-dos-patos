import abc

class Pato():
    def __init__(self):
        self.comportamento = None

    @abc.abstractmethod
    def mostrar(self):
        pass

    def nadar(self):
        return "Pato Nadando"
    
    def setComportamento(self, padrao):
        self.comportamento = padrao
    
    def comportamentoPato(self):
        return self.comportamento.voar()



