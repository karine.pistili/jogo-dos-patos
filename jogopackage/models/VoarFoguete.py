from models.PadraoVoaveis import PadraoVoaveis

class VoarFoguete(PadraoVoaveis):
    def __init__(self):
        self.velocidade = 1000

    def voar(self):
        return f'Voando como um foguete. Velocidade: {self.getVelocidade()}'

    def getVelocidade(self):
        return self.velocidade