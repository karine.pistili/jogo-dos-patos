
from models.PadraoVoaveis import PadraoVoaveis 

class NaoVoa(PadraoVoaveis):
    ##implement PadraoVoaveis
    def __init__(self):
        pass

    def voar(self):
        return "Esse pato não voa. Velocidade: " + self.getVelocidade()
    
    def getVelocidade(self):
        return 0