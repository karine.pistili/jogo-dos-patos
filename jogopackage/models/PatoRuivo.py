from models.Pato import Pato
from models.PadraoGrasnar import PadraoGrasnar
from models.VoaveisAsa import VoaveisAsa

class PatoRuivo (Pato, PadraoGrasnar):
    def __init__(self):
        self.setComportamento(VoaveisAsa())
    
    def mostrar(self):
        return "Eu sou o Pato Ruivo"

    def grasnar(self):
        return "Que-Que."