from models.Pato import Pato
from models.VoaveisAsa import VoaveisAsa
from models.PadraoGrasnar import PadraoGrasnar

class PatoBravo(Pato, PadraoGrasnar):
    def __init__(self):
        self.setComportamento(VoaveisAsa())

    def mostrar(self):
        return "Eu sou o Pato Bravo"
    
    def grasnar(self):
        return "Que-Que. Grrrrrrrrrrr"